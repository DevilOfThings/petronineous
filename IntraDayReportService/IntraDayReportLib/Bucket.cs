﻿using Services;
using System;

namespace IntraDayReportLib
{
    /// <summary>
    /// 
    /// </summary>
    public class Bucket : IBucket
    {
        /// <summary>
        /// Period for the bucket
        /// </summary>
        public int Period { get; set ; }
        /// <summary>
        /// Current Violume for this bucket
        /// </summary>
        public double Volume { get; set; }

    }
}