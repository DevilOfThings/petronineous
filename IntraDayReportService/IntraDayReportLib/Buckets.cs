﻿using IntraDayReportLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Services;

namespace IntraDayReportLib
{
    /// <summary>
    /// Buckets container for the bucket periods
    /// </summary>
    public class Buckets : IBuckets
    {
        const int periods = 24;

        /// <summary>
        /// 
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Trades collection
        /// </summary>
        public Bucket[] Trades { get; set; } = new Bucket[periods];

        /// <summary>
        /// constructs the internal container
        /// </summary>
        public Buckets()
        {
            for (int i = 0; i < periods; ++i)
            {
                Trades[i] = new Bucket { Period = i+1 };
            }
        }

        /// <summary>
        /// Add the power periods to the bucket
        /// </summary>
        /// <param name="trade"></param>
        public void Add(PowerPeriod trade)
        {
            if (trade.Period < 1 || trade.Period > 24)
                throw new IndexOutOfRangeException($"Period {trade.Period} is not in the range 1 to 24");
            Trades[trade.Period-1].Volume += trade.Volume;
        }

        
    }
}
