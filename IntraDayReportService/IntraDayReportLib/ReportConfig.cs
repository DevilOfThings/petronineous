﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntraDayReportLib
{
    public class ReportConfig : IReportConfig
    {
        /// <summary>
        /// Report location
        /// </summary>
        public DirectoryInfo ReportDir { get => new DirectoryInfo(ConfigurationManager.AppSettings["ReportDir"]); }

        /// <summary>
        /// 
        /// </summary>
        public DirectoryInfo LogDir { get => new DirectoryInfo(ConfigurationManager.AppSettings["LogDir"]); }


        /// <summary>
        /// Interval times
        /// </summary>
        public int Interval { get
            {
                Int32.TryParse(ConfigurationManager.AppSettings["Interval"], out int interval);
                return (interval * 1000) - IntervalDiff; // +/- 1 minute
            }
        }

        /// <summary>
        /// max interval difference
        /// </summary>
        public int IntervalDiff
        {
            get
            {
                Int32.TryParse(ConfigurationManager.AppSettings["MaxIntervalDiff"], out int intervalDiff);
                return intervalDiff; 
            }
        }
    }
}
