﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntraDayReportLib
{
    /// <summary>
    /// IReportConfig interface
    /// </summary>
    interface IReportConfig
    {
        DirectoryInfo ReportDir { get; }
        DirectoryInfo LogDir { get;  }

        /// <summary>
        /// Interval in minutes
        /// </summary>
        int Interval { get; } 

        /// <summary>
        /// +/- allowed interval discrepancy in minutes
        /// </summary>
        int IntervalDiff { get; }
    }
}
