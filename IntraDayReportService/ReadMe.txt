
IntraDayReport.UT
This is the unit test project

IntradDayReportLib
This is the API for the IntraDay reports

IntraDayReportService
This is the windows service, can be debugged as console application as well as run as a service if it has been installed

To install as a service, run powershell as administrator
"New-Service -Name \"Petroineous\" -BinaryPath \"<path to executable>\" "

to uninstall teh service
"(Get-WmiObject Win32_service -filter \"name = 'Petroineous'\").Delete()"


CaptureRun.png is a screenshort of the Petroineous running as a service with tail of the log file
The reports are set to be stored inC:\Temp\Petroineous\Reports and the log is in C:\Temp\Petroineous\Log, these directories can be changed by changing the app.config file