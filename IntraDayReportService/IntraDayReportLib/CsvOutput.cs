﻿using IntraDayReportLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntraDayReportLib
{
    /// <summary>
    /// 
    /// </summary>
    public static class CsvOutput 
    {

        /// <summary>
        /// Configures the intraday report name
        /// </summary>
        const string PartialFileName = "PowerPosition_";
        public static string GetCsvFileName(this DateTime date)
        {
            return PartialFileName + $"{date.ToString("yyyyMMdd")}_{date.ToString("HHmm")}.csv";
        }
    }
}
