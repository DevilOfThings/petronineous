﻿using Services;
using System;

namespace IntraDayReportLib
{
    /// <summary>
    /// IBucket interface
    /// </summary>
    internal interface IBucket
    {
        /// <summary>
        /// Current priod for this bucket
        /// </summary>
        int Period { get; set; }

        /// <summary>
        /// Current volume for this bucket
        /// </summary>
        double Volume { get; set; }
    }
}