﻿using IntraDayReportLib.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;
using log4net;
using System.Diagnostics;

namespace IntraDayReportLib
{
    /// <summary>
    /// Extraction class for generating intraday power trade reports
    /// configured through app.config
    /// </summary>
    public class Extraction : IExtraction
    {
        private static ILog Log =LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Timer Class for handling OnTimed Events
        /// </summary>
        public Timer ExecutionTimer { get; set; } = new Timer();
        public TimeSpan MaxIntervalDiff { get; set;  }

        private ReportConfig Config = new ReportConfig();

        /// <summary>
        /// Initialise the extraction report
        /// </summary>
        public void Initialise()
        { 
            Directory.CreateDirectory(Config.LogDir.FullName);
            Directory.CreateDirectory(Config.ReportDir.FullName);

            ExecutionTimer.Interval = Config.Interval;
            MaxIntervalDiff = TimeSpan.FromMinutes(Config.IntervalDiff);
            ExecutionTimer.Start();
        }


        /// <summary>
        /// Run the Intraday report, output to directory set in app.config
        /// Logs to directory set in app.config
        /// </summary>
        public void RunReport()
        {
            Log.Debug("Starting run...");

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var buckets = new Buckets() { Date = DateTime.Now };
            var fileName = Config.ReportDir + CsvOutput.GetCsvFileName(buckets.Date);

            try
            {
                ExecutionTimer.Enabled = false;

                var service = new Services.PowerService();
                var trades = service.GetTrades(buckets.Date);

                using (var stream = new FileStream(fileName, FileMode.CreateNew))
                using (var csvStream = new StreamWriter(stream))
                {
                    csvStream.WriteLine("Local Time, Volume");

                    foreach (var trade in trades)
                    {
                        foreach (var period in trade.Periods)
                        {
                            buckets.Add(period);
                        }
                    }

                    Console.WriteLine($"Date:{buckets.Date}");
                    Log.Info($"found {trades.Count()} trades for date:{buckets.Date}");

                    foreach (var bucket in buckets.Trades)
                    {
                        Console.WriteLine($"{bucket.Period.ToLocalTime()}, {bucket.Volume}");
                        csvStream.WriteLine($"{bucket.Period.ToLocalTime()}, {bucket.Volume}");
                    }

                    stopwatch.Stop();
                    if (stopwatch.Elapsed > MaxIntervalDiff)
                    {
                        Log.Warn($"Last report over ran the allowed time period {stopwatch.Elapsed}");
                    }
                }
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e);
                Log.Error(e.Message);
                throw;
            }
            catch (Services.PowerServiceException e)
            {
                Console.WriteLine(e);
                Log.Error(e.Message);
                // if PowerServiceException is thrown everytime could end up in a continuous loop
                // TODO: add a check to detect if this occurs multiple times
                RunReport();
            }
            catch (IOException e)
            {
                var exists = File.Exists(fileName);
                Console.WriteLine(e);
                Log.Error(e.Message);
                // log the error and report to production support but continue
                //throw;
            }
            finally
            {
                ExecutionTimer.Enabled = true;
            }
        }

        /// <summary>
        /// start called from the service
        /// </summary>
        public void Start()
        {
            Initialise();
            RunReport();
            StartTimer();
        }

        /// <summary>
        /// stops the service
        /// </summary>
        public void Stop()
        {
            KillTimer();

            // do other needed cleanup here...
        }

        /// <summary>
        ///  start the timer event
        /// </summary>
        public void StartTimer()
        {
            Log.Debug("Starting timer");
            ExecutionTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            ExecutionTimer.Enabled = true;
        }

        /// <summary>
        ///  kill the timer event
        /// </summary>
        public void KillTimer()
        {
            Log.Debug("Pausing timer...");
            ExecutionTimer.Elapsed -= new ElapsedEventHandler(OnTimedEvent);
            ExecutionTimer.Enabled = false;
        }

        /// <summary>
        /// OnTimedEvent called based on the interval value set in the app.config
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            RunReport();
        }
    }
}
