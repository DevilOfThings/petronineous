﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services;
using System.IO;
using IntraDayReportLib;
using System.Configuration;
using System.Timers;
using log4net;

namespace IntraDayReport.UT
{
    /// <summary>
    /// Tests for the intrad day reporting classes
    /// </summary>
    [TestClass]
    public class TradePositionTests
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string FileName;
        /// <summary>
        /// Extraction class to be tested
        /// </summary>
        private static Extraction Extraction;

        [ClassInitialize]
        public static void ClassSetup(TestContext context)
        {
            // create class to be tested once per test suite
            Extraction = new Extraction();
        }

        [TestInitialize]
        public void Setup()
        {
            // add any test set up here
            FileName = new ReportConfig().ReportDir + CsvOutput.GetCsvFileName(DateTime.Now);

        }

        [TestCleanup]
        public void Teardown()
        {
            // add any tear down code required here, otherwise tests will fail as already exists for current timne
            if (File.Exists(FileName))
            {
                File.Delete(FileName);
            }
        }

        [TestMethod]
        public void GetTrades_Test()
        {
            try
            {
                var service = new Services.PowerService();

                var trades = service.GetTrades(DateTime.Today);
                foreach (var trade in trades)
                {
                    Console.WriteLine($"Date:{trade.Date}");
                    foreach (var period in trade.Periods)
                    {
                        Console.WriteLine($"Period:{period.Period} Volume:{period.Volume}");
                    }
                }
                Assert.IsTrue(trades.Any());
            }
            catch (Services.PowerServiceException e)
            {
                Console.WriteLine(e);
                // dont re-throw as we have no control over when the PowerServiceException will be thrown
                //throw;
            }
        }

        [TestMethod]
        public void GetTradesAsync_Test()
        {
            try
            {
                var service = new Services.PowerService();

                var trades = service.GetTradesAsync(DateTime.Today).GetAwaiter().GetResult();
                foreach (var trade in trades)
                {
                    Console.WriteLine($"Date:{trade.Date}");
                    foreach (var period in trade.Periods)
                    {
                        Console.WriteLine($"Period:{period.Period} Volume:{period.Volume}");
                    }
                }
                Assert.IsTrue(trades.Any());
            }
            catch(Services.PowerServiceException e)
            {
                Console.WriteLine(e);
                // dont re-throw as we have no control over when the PowerServiceException will be thrown
                //throw;
            }

        }



        [TestMethod]
        public void OutputCsv_Test()
        {
            Log.Debug("OutputCsv_Test");

            
            Extraction.Initialise();
            Extraction.RunReport();
        }

        #region Extraction Tests
        [TestMethod]
        public void Initialise_Test()
        {
            Extraction.Initialise();
        }

        [TestMethod]
        public void RunReport_Test()
        {
            var dirInfo = new ReportConfig().ReportDir;
            var filesOrig = dirInfo.GetFiles();

            Extraction.RunReport();

            var filesNow = dirInfo.GetFiles();
            filesNow = dirInfo.GetFiles();
            Assert.IsTrue(filesOrig.Count() < filesNow.Count());
        }

        [TestMethod]
        public void Start_Test()
        {
            Extraction.Start();
            Assert.IsTrue(Extraction.ExecutionTimer.Enabled);

        }

        [TestMethod]
        public void Stop_Test()
        {
            Extraction.Stop();
            Assert.IsFalse(Extraction.ExecutionTimer.Enabled);
        }

        [TestMethod]
        public void StartStopTimer_Test()
        {
            Extraction.StartTimer();
            Assert.IsTrue(Extraction.ExecutionTimer.Enabled);
            
            Extraction.KillTimer();
            Assert.IsFalse(Extraction.ExecutionTimer.Enabled);
        }



        [TestMethod]
        public void OnTimedEvent_Test()
        {
            var dirInfo = new ReportConfig().ReportDir;
            var filesOrig = dirInfo.GetFiles();
            // we know we dont use arguments so null is okay, but we could modify to use ElapsedEventsArgs to fine tune report runs
            Extraction.OnTimedEvent(null, null);
            var filesNow = dirInfo.GetFiles();
            filesNow = dirInfo.GetFiles();
            Assert.IsTrue(filesOrig.Count() < filesNow.Count());
        }
        #endregion extraction tests
    }
}
