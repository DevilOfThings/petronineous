﻿using IntraDayReportLib;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;




namespace IntraDayReportService
{
    public partial class ReportService : ServiceBase
    {
        private static ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        Extraction Extraction = new Extraction();

        public static void Start(string[] args)
        {
            var service = new ReportService();

            if (Environment.UserInteractive)
            {
                var installSwitch = (from s in args where s.ToLower() == "/i" select s).FirstOrDefault();
                var uninstallSwitch = (from s in args where s.ToLower() == "/u" select s).FirstOrDefault();

                if (string.IsNullOrWhiteSpace(installSwitch) && string.IsNullOrWhiteSpace(uninstallSwitch))
                {
                    service.OnStart(args);
                    Console.WriteLine("Press any key to stop  program");
                    Console.Read();
                    service.OnStop();
                }
                else if (installSwitch !=null)
                {
                    // TODO: Allow creating service through console , in the meantime use powershell to install the service or use installutil.exe if available
                    Console.WriteLine(@"Use powershell in admin mode to delete the service");
                    Console.WriteLine("New-Service -Name \"Petroineous\" -BinaryPath \"<path to executable>\" ");                    
                    WindowsServiceHelper.InstallService();
                }
                else if (uninstallSwitch !=null)
                {
                    // TODO: Allow creating service through console , in the meantime use powershell to un-install the service or use installutil.exe if available
                    Console.WriteLine(@"Use powershell in admin mode to delete the service, depending on pwershell version installed");
                    Console.WriteLine("Remove-Service -Name \"Petroineous\"");
                    Console.WriteLine("(Get-WmiObject Win32_service -filter \"name = 'Petroineous'\").Delete()");
                    WindowsServiceHelper.UnInstallService();
                }
            }
            else
            {
                var servicesToRun = new ServiceBase[]
                {
                    service
                };
                ServiceBase.Run(servicesToRun);

            }
        }
        public ReportService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Extraction.Start();
        }

        protected override void OnStop()
        {
            Task.Run(() =>
            {
                Extraction.Stop();
            });
        }
    }
}
