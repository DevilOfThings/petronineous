﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntraDayReportLib
{
    /// <summary>
    /// 
    /// </summary>
    public static class LocalTime
    {

        /// <summary>
        /// Get the LocalTime for this period
        /// </summary>
        /// <param name="period"></param>
        /// <returns></returns>
        public static string ToLocalTime(this int period)
        {
            if (period < 1 || period > 24)
                throw new IndexOutOfRangeException();

            var localTime = new [] {
                "23:00", "00:00", "01:00", "02:00", "03:00", "04:00",
                "05:00", "06:00", "07:00", "08:00", "09:00", "10:00",
                "11:00", "12:00", "13:00", "14:00", "15:00", "16:00",
                "17:00", "18:00", "19:00", "20:00", "21:00", "22:00"
            };

            return localTime[period-1];
        }

        
    }
}
