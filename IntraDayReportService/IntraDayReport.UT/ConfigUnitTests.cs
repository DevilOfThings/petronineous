﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using IntraDayReportLib;

namespace IntraDayReport.UT
{
    /// <summary>
    /// Tests for the configuration settings
    /// </summary>
    [TestClass]
    public class ConfigUnitTests
    {
        [TestMethod]
        public void ReportDir_Test()
        {
            Assert.IsNotNull(new ReportConfig().ReportDir);
        }

        [TestMethod]
        public void LogDirTest()
        {
            Assert.IsNotNull(new ReportConfig().ReportDir);
        }

        [TestMethod]
        public void Interval_Test()
        {
            var test = new ReportConfig().Interval;
            Assert.IsNotNull(test);
            Assert.IsTrue(test > 0);
        }

        [TestMethod]
        public void MaxInterval_Test()
        {
            var test = new ReportConfig().IntervalDiff;
            Assert.IsNotNull(test);
            Assert.IsTrue(test > 0);
        }
    }
}
