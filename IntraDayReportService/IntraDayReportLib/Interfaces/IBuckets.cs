﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntraDayReportLib.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    interface IBuckets
    {
        /// <summary>
        /// Collection of the trades
        /// </summary>
        Bucket[] Trades { get; set; }

        /// <summary>
        /// Add to this bucket
        /// </summary>
        /// <param name="trade"></param>
        void Add(PowerPeriod trade);
    }
}
