﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace IntraDayReportLib.Interfaces
{
    /// <summary>
    /// IExtraction interface
    /// </summary>
    interface IExtraction
    {
        /// <summary>
        /// Intialise the extraction process
        /// </summary>
        void Initialise();

        /// <summary>
        /// Run the report
        /// </summary>
        void RunReport();
        
        /// <summary>
        /// Start the timer
        /// </summary>
        void StartTimer();

        /// <summary>
        /// Kill the timer
        /// </summary>
        void KillTimer();

        /// <summary>
        /// OnTimedEvent delegate is called when the time interval period has been met
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnTimedEvent(object sender, ElapsedEventArgs e);
    }
}
